<?php

namespace ViewPage;

function create ($user, &$scripts) {

    $id = $user->id_users;
    $fnsDir = __DIR__.'/../../../../fns';

    include_once "$fnsDir/compressed_js_script.php";
    $scripts = compressed_js_script('dateAgo', '../../../');

    include_once "$fnsDir/export_date_ago.php";

    include_once "$fnsDir/request_strings.php";
    list($keyword) = request_strings('keyword');

    include_once "$fnsDir/parse_keyword.php";
    parse_keyword($keyword, $includes, $excludes);

    $username = htmlspecialchars($user->username);
    if ($includes) {
        include_once "$fnsDir/keyword_regex.php";
        $regex = keyword_regex($includes);
        $username = preg_replace($regex, '<mark>$0</mark>', $username);
    }

    $access_time = $user->access_time;
    if ($access_time === null) $accessed = 'Never';
    else {

        $accessed = export_date_ago($access_time, true);

        $access_remote_address = $user->access_remote_address;
        if ($access_remote_address !== null) {
            $accessed .= ' from '.htmlspecialchars($access_remote_address);
        }

    }

    include_once __DIR__.'/../../../fns/format_author.php';
    $author = format_author($user->insert_time, $user->insert_api_key_name);
    $infoText = '';
    if ($user->disabled) $infoText .= 'Disabled.<br />';
    if ($user->expires) {

        include_once "$fnsDir/Users/emailExpireDays.php";
        include_once "$fnsDir/Users/expireDays.php";
        $expireDays = \Users\emailExpireDays() + \Users\expireDays();

        $infoText .= "Will expires when inactive for $expireDays days.<br />";

    }
    $infoText .= "User created $author.";

    include_once __DIR__.'/unsetSessionVars.php';
    unsetSessionVars();

    include_once __DIR__.'/optionsPanel.php';
    include_once "$fnsDir/create_new_item_button.php";
    include_once "$fnsDir/Form/label.php";
    include_once "$fnsDir/ItemList/listHref.php";
    include_once "$fnsDir/Page/create.php";
    include_once "$fnsDir/Page/infoText.php";
    include_once "$fnsDir/Page/sessionMessages.php";
    include_once "$fnsDir/UserViewPage/emailItem.php";
    include_once "$fnsDir/UserViewPage/fullNameItem.php";
    include_once "$fnsDir/UserViewPage/signinsItem.php";
    include_once "$fnsDir/UserViewPage/storageItem.php";
    include_once "$fnsDir/UserViewPage/themeItem.php";
    include_once "$fnsDir/UserViewPage/timezoneItem.php";
    return
        \Page\create(
            [
                'title' => 'Users',
                'href' => \ItemList\listHref()."#$id",
            ],
            "User #$id",
            \Page\sessionMessages('admin/users/view/messages')
            .\Form\label('Username', $username)
            .\UserViewPage\emailItem($user)
            .\UserViewPage\fullNameItem($user)
            .\UserViewPage\timezoneItem($user)
            .\UserViewPage\themeItem($user)
            .\UserViewPage\storageItem($user)
            .\UserViewPage\signinsItem($user)
            .'<div class="hr"></div>'
            .\Form\label('Last accessed', $accessed)
            .\Page\infoText($infoText),
            create_new_item_button('User', '../')
        )
        .optionsPanel($id);

}
