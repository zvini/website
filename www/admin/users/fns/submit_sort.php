<?php

function submit_sort ($order_by, $what) {

    $fnsDir = __DIR__.'/../../../fns';

    include_once "$fnsDir/require_same_domain_referer.php";
    require_same_domain_referer('./');

    include_once __DIR__.'/../../fns/require_admin.php';
    require_admin();

    include_once "$fnsDir/Users/OrderBy/set.php";
    $ok = Users\OrderBy\set($order_by);

    if ($ok === false) {
        unset($_SESSION['admin/users/messages']);
        $_SESSION['admin/users/errors'] = ['Failed to save the sort field.'];
    } else {
        unset($_SESSION['admin/users/errors']);
        $_SESSION['admin/users/messages'] = [
            "The list is now sorted by $what.",
        ];
    }

    include_once "$fnsDir/redirect.php";
    include_once "$fnsDir/ItemList/listUrl.php";
    redirect(ItemList\listUrl('./'));

}
