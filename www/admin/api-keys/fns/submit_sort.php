<?php

function submit_sort ($order_by, $what) {

    $fnsDir = __DIR__.'/../../../fns';

    include_once "$fnsDir/require_same_domain_referer.php";
    require_same_domain_referer('./');

    include_once __DIR__.'/../../fns/require_admin.php';
    require_admin();

    include_once "$fnsDir/AdminApiKeys/OrderBy/set.php";
    $ok = AdminApiKeys\OrderBy\set($order_by);

    if ($ok === false) {
        unset($_SESSION['admin/api-keys/messages']);
        $_SESSION['admin/api-keys/errors'] = ['Failed to save the sort field.'];
    } else {
        unset($_SESSION['admin/api-keys/errors']);
        $_SESSION['admin/api-keys/messages'] = [
            "The list is now sorted by $what.",
        ];
    }

    include_once "$fnsDir/redirect.php";
    include_once "$fnsDir/ItemList/listUrl.php";
    redirect(ItemList\listUrl('./'));

}
