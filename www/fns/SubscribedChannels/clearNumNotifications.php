<?php

namespace SubscribedChannels;

function clearNumNotifications ($mysqli, $id) {
    $sql = 'update subscribed_channels set num_notifications = 0'
        ." where id = $id";
    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);
}
