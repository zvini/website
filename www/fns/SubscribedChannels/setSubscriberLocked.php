<?php

namespace SubscribedChannels;

function setSubscriberLocked ($mysqli, $id, $subscriber_locked) {
    $subscriber_locked = $subscriber_locked ? '1' : '0';
    $sql = 'update subscribed_channels'
        ." set subscriber_locked = $subscriber_locked"
        ." where id = $id";
    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);
}
