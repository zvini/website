<?php

namespace SubscribedChannels;

function clearNumNotificationsOnSubscriber ($mysqli, $subscriber_id_users) {
    $sql = 'update subscribed_channels set num_notifications = 0'
        ." where subscriber_id_users = $subscriber_id_users";
    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);
}
