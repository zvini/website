<?php

namespace SubscribedChannels;

function setPublisherLocked ($mysqli, $id, $publisher_locked) {
    $publisher_locked = $publisher_locked ? '1' : '0';
    $sql = 'update subscribed_channels'
        ." set publisher_locked = $publisher_locked where id = $id";
    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);
}
