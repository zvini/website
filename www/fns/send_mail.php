<?php

function send_mail ($to, $subject, $html, $options = []) {

    include_once __DIR__ . '/DomainName/get.php';
    $headers =
        'From: no-reply@'.DomainName\get()."\r\n"
        .'Content-Type: text/html; charset=UTF-8';
    if (array_key_exists('reply_to', $options)) {
        $headers .= "\r\nReply-To: $options[reply_to]";
    }

    mail($to, $subject, $html, $headers);

}
