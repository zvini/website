<?php

namespace Users\Files;

function rename ($mysqli, $file, $name, &$changed, $renameApiKey = null) {

    if ($file->name === $name) return;

    $changed = true;

    include_once __DIR__.'/../../Files/rename.php';
    \Files\rename($mysqli, $file->id_files, $name, $renameApiKey);

}
