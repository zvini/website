<?php

namespace Users\Schedules;

function invalidate ($mysqli, $id_users) {
    $sql = 'update users set schedules_check_day = 0, num_schedules_today = 0,'
        ." num_schedules_tomorrow = 0 where id_users = $id_users";
    include_once __DIR__.'/../../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);
}
