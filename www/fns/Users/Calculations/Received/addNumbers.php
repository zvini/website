<?php

namespace Users\Calculations\Received;

function addNumbers ($mysqli, $id_users, $num, $num_archived) {
    $sql = 'update users set'
        ." num_received_calculations = num_received_calculations + $num,"
        .' num_archived_received_calculations'
        ." = num_archived_received_calculations + $num_archived"
        ." where id_users = $id_users";
    include_once __DIR__.'/../../../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);
}
