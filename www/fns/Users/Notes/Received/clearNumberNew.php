<?php

namespace Users\Notes\Received;

function clearNumberNew ($mysqli, $user) {
    $user->home_num_new_received_notes = 0;
    $sql = 'update users set home_num_new_received_notes = 0'
        ." where id_users = $user->id_users";
    include_once __DIR__.'/../../../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);
}
