<?php

namespace Users\Folders;

function rename ($mysqli, $folder, $name, &$changed, $renameApiKey = null) {

    if ($folder->name === $name) return;

    $changed = true;

    include_once __DIR__.'/../../Folders/rename.php';
    \Folders\rename($mysqli, $folder->id_folders, $name, $renameApiKey);

}
