<?php

namespace Users;

function isVerifyEmailPending ($user) {

    if (!$user->verify_email_key) return false;

    include_once __DIR__.'/verifyEmailKeyExpireHours.php';
    $expireTime = verifyEmailKeyExpireHours() * 60 * 60;

    return $user->verify_email_key_time > time() - $expireTime;

}
