<?php

namespace Users\ResetPasswordKeys;

function delete ($mysqli, $id_users, $id) {

    include_once __DIR__.'/../../ResetPasswordKeys/delete.php';
    \ResetPasswordKeys\delete($mysqli, $id);

    include_once __DIR__.'/addNumber.php';
    addNumber($mysqli, $id_users, -1);

}
