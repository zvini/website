<?php

namespace Users\ResetPasswordKeys;

function add ($mysqli, $user, $key, $return, $insert_time) {

    $id_users = $user->id_users;

    include_once __DIR__.'/../../ResetPasswordKeys/add.php';
    \ResetPasswordKeys\add($mysqli, $id_users, $key, $return, $insert_time);

    include_once __DIR__.'/addNumber.php';
    addNumber($mysqli, $id_users, 1);

}
