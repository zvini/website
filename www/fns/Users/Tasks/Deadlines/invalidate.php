<?php

namespace Users\Tasks\Deadlines;

function invalidate ($mysqli, $id_users) {
    $sql = 'update users set task_deadlines_check_day = 0,'
        .' num_task_deadlines_today = 0, num_task_deadlines_tomorrow = 0'
        ." where id_users = $id_users";
    include_once __DIR__.'/../../../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);
}
