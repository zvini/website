<?php

namespace Wallets;

function removeAllTransactions ($mysqli, $id) {
    $sql = 'update wallets set num_transactions = 0,'
        ." income = 0, expense = 0, balance = 0 where id = $id";
    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);
}
