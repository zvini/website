<?php

namespace Wallets;

function addDeleted ($mysqli, $id, $id_users, $name, $income, $expense,
    $balance, $num_transactions, $insert_time, $update_time, $revision) {

    $name = $mysqli->real_escape_string($name);

    $sql = 'insert into wallets (id, id_users, name, income, expense, balance,'
        .' num_transactions, insert_time, update_time, revision)'
        ." values ($id, $id_users, '$name', $income, $expense, $balance,"
        ." $num_transactions, $insert_time, $update_time, $revision)";

    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);

}
