<?php

function create_calendar_icon_today ($user, $additionalClass = '') {
    include_once __DIR__.'/user_time_today.php';
    return
        "<span class=\"icon calendar$additionalClass\">"
            .'<span class="calendarIcon-day calendarIcon-today">'
                .date('j', user_time_today($user))
            .'</span>'
        .'</span>';
}
