<?php

namespace AdminConnectionAuths;

function deleteOlder ($mysqli, $insert_time) {
    $sql = 'delete from admin_connection_auths'
        ." where insert_time < $insert_time";
    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);
}
