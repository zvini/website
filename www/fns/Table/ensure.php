<?php

namespace Table;

function ensure ($mysqli, $tableName, $columns) {

    $fnsDir = __DIR__.'/..';

    include_once "$fnsDir/mysqli_single_object.php";
    $db = mysqli_single_object($mysqli, 'select database() db')->db;

    $escapedTableName = $mysqli->real_escape_string($tableName);

    include_once __DIR__.'/columnDefinition.php';

    include_once __DIR__.'/get.php';
    $table = get($mysqli, $db, $tableName);

    $output = '';

    if ($table) {

        include_once __DIR__.'/columns.php';
        $existingColumns = columns($mysqli, $db, $tableName);

        foreach ($existingColumns as $existingColumn) {

            $columnName = $existingColumn->COLUMN_NAME;
            if (!array_key_exists($columnName, $columns)) continue;

            $existingType = $existingColumn->COLUMN_TYPE;
            $existingNullable = $existingColumn->IS_NULLABLE === 'YES';
            $existingPrimary = $existingColumn->COLUMN_KEY == 'PRI';
            $existingIncrement = $existingColumn->EXTRA == 'auto_increment';
            $existingCharset = $existingColumn->CHARACTER_SET_NAME;
            $existingCollation = $existingColumn->COLLATION_NAME;

            $column = $columns[$columnName];
            $type = $column['type'];
            $primary = array_key_exists('primary', $column);
            unset($columns[$columnName]);

            if (array_key_exists('nullable', $column)) {
                $nullable = $column['nullable'];
            } else {
                $nullable = false;
            }

            if (array_key_exists('characterSet', $column)) {
                $charset = $column['characterSet'];
                $collation = $column['collation'];
            } else {
                $charset = $collation = null;
            }

            if ($primary) $primaryOk = $existingPrimary && $existingIncrement;
            else $primaryOk = !$existingPrimary && !$existingIncrement;

            if ($type === $existingType &&
                $nullable === $existingNullable &&
                $charset === $existingCharset &&
                $collation === $existingCollation && $primaryOk) continue;

            if ($existingPrimary) {
                $sql = "alter table `$escapedTableName` drop primary key";
                $output .= "SQL: $sql\n";
                include_once "$fnsDir/mysqli_query_exit.php";
                mysqli_query_exit($mysqli, $sql);
            }

            $escapedColumnName = $mysqli->real_escape_string($columnName);
            $sql = "alter table `$escapedTableName` change `$escapedColumnName`"
                ." `$escapedColumnName` ".columnDefinition($column);

            $output .= "SQL: $sql\n";

            mysqli_query_exit($mysqli, $sql);

        }

        include_once __DIR__.'/addColumns.php';
        $output .= addColumns($mysqli, $tableName, $columns);

    } else {
        include_once __DIR__.'/create.php';
        $output .= create($mysqli, $tableName, $columns);
    }

    if ($output === '') {
        $output .= "Nothing to do for the table \"$tableName\".\n";
    }

    return $output;

}
