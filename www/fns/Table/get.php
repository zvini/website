<?php

namespace Table;

function get ($mysqli, $db, $tableName) {

    $db = $mysqli->real_escape_string($db);
    $tableName = $mysqli->real_escape_string($tableName);

    $sql = 'select * from information_schema.tables'
        ." where table_schema = '$db' and table_name = '$tableName'";

    include_once __DIR__.'/../mysqli_single_object.php';
    return mysqli_single_object($mysqli, $sql);

}
