<?php

namespace Table;

function create ($mysqli, $tableName, $columns) {

    $escapedTableName = $mysqli->real_escape_string($tableName);

    include_once __DIR__.'/columnDefinition.php';
    $sql = "create table `$escapedTableName` (";
    $first = true;
    foreach ($columns as $name => $column) {
        if ($first) $first = false;
        else $sql .= ', ';
        $escapedName = $mysqli->real_escape_string($name);
        $definition = columnDefinition($column);
        $sql .= "`$escapedName` $definition";
    }
    $sql .= ')';

    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);

    return "SQL: $sql\n";

}
