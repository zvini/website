<?php

namespace ResetPasswordKeys;

function ensure ($mysqli) {
    include_once __DIR__.'/../ApiKey/column.php';
    include_once __DIR__.'/../Table/ensure.php';
    return \Table\ensure($mysqli, 'reset_password_keys', [
        'id' => [
            'type' => 'bigint(20) unsigned',
            'primary' => true,
        ],
        'id_users' => [
            'type' => 'bigint(20) unsigned',
        ],
        'insert_time' => [
            'type' => 'bigint(20) unsigned',
        ],
        'key' => \ApiKey\column(),
        'return' => [
            'type' => 'varchar(2048)',
            'characterSet' => 'utf8',
            'collation' => 'utf8_unicode_ci',
        ],
    ]);
}
