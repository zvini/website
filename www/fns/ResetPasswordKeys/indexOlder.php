<?php

namespace ResetPasswordKeys;

function indexOlder ($mysqli, $insert_time) {
    $sql = 'select * from reset_password_keys'
        ." where insert_time < $insert_time";
    include_once __DIR__.'/../mysqli_query_object.php';
    return mysqli_query_object($mysqli, $sql);
}
