<?php

namespace ResetPasswordKeys;

function add ($mysqli, $id_users, $key, $return, $insert_time) {
    $key = $mysqli->real_escape_string($key);
    $return = $mysqli->real_escape_string($return);
    $sql = 'insert into reset_password_keys'
        .' (id_users, `key`, `return`, insert_time)'
        ." values ($id_users, '$key', '$return', $insert_time)";
    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);
}
