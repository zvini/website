<?php

namespace CrossSiteApiKeys;

function deleteOlder ($mysqli, $insert_time) {
    $sql = 'delete from cross_site_api_keys'
        ." where insert_time < $insert_time";
    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);
}
