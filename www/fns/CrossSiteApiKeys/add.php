<?php

namespace CrossSiteApiKeys;

function add ($mysqli, $id_users, $username, $key) {
    $key = $mysqli->real_escape_string($key);
    $username = $mysqli->real_escape_string($username);
    $insert_time = time();
    $sql = 'insert into cross_site_api_keys'
        .' (id_users, `key`, username, insert_time)'
        ." values ($id_users, '$key', '$username', $insert_time)";
    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);
    return $mysqli->insert_id;
}
