<?php

namespace Feedbacks;

function add ($mysqli, $id_users, $text) {

    if ($id_users === null) $id_users = 'null';
    $text = $mysqli->real_escape_string($text);
    $insert_time = time();

    $sql = 'insert into feedbacks'
        .' (id_users, text, insert_time)'
        ." values ($id_users, '$text', $insert_time)";
    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);
    return $mysqli->insert_id;

}
