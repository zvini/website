<?php

namespace Channels;

function clearNumNotificationsOnUser ($mysqli, $id_users) {
    $sql = "update channels set num_notifications = 0"
        ." where id_users = $id_users";
    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);
}
