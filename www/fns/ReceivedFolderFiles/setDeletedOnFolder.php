<?php

namespace ReceivedFolderFiles;

function setDeletedOnFolder ($mysqli, $id_received_folders, $deleted) {
    $deleted = $deleted ? '1' : '0';
    $sql = "update received_folder_files set deleted = $deleted"
        ." where id_received_folders = $id_received_folders";
    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);
}
