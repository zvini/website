<?php

namespace ScheduleRevisions;

function setDeletedOnSchedule ($mysqli, $id_schedules, $deleted) {
    $deleted = $deleted ? '1' : '0';
    $sql = "update schedule_revisions set deleted = $deleted"
        ." where id_schedules = $id_schedules";
    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);
}
