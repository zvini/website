<?php

namespace ScheduleRevisions;

function setDeletedOnUser ($mysqli, $id_users) {
    $sql = 'update schedule_revisions set'
        ." deleted = 1 where id_users = $id_users";
    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);
}
