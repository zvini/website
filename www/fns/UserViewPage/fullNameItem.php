<?php

namespace UserViewPage;

function fullNameItem ($user) {

    $full_name = $user->full_name;
    if ($full_name === '') return;

    include_once __DIR__.'/../Form/label.php';
    return '<div class="hr"></div>'.\Form\label('Full name', $full_name);

}
