<?php

namespace UserViewPage;

function signinsItem ($user) {
    include_once __DIR__.'/../n_times.php';
    include_once __DIR__.'/../Form/label.php';
    return '<div class="hr"></div>'
        .\Form\label('Signed in', ucfirst(\n_times($user->num_signins)));
}
