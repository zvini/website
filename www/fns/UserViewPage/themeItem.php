<?php

namespace UserViewPage;

function themeItem ($user) {

    include_once __DIR__.'/../Theme/Brightness/index.php';
    include_once __DIR__.'/../Theme/Color/index.php';
    $value = \Theme\Color\index()[$user->theme_color].' - '
        .\Theme\Brightness\index()[$user->theme_brightness]['title'];

    include_once __DIR__.'/../Form/label.php';
    return '<div class="hr"></div>'.\Form\label('Theme', $value);

}
