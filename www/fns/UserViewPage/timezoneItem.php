<?php

namespace UserViewPage;

function timezoneItem ($user) {

    $timezone = $user->timezone;
    if (!$timezone) return;

    include_once __DIR__.'/../Form/label.php';
    include_once __DIR__.'/../Timezone/format.php';
    return '<div class="hr"></div>'
        .\Form\label('Timezone', \Timezone\format($timezone));

}
