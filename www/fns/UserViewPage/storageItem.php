<?php

namespace UserViewPage;

function storageItem ($user) {
    include_once __DIR__.'/../bytestr.php';
    include_once __DIR__.'/../Form/label.php';
    return '<div class="hr"></div>'
        .\Form\label('Using storage', bytestr($user->storage_used));
}
