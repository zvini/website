<?php

namespace UserViewPage;

function emailItem ($user) {

    $email = $user->email;
    if ($email === '') return;

    if ($user->email_verified) $status = 'Verified';
    else {
        include_once __DIR__.'/../Users/isVerifyEmailPending.php';
        if (\Users\isVerifyEmailPending($user)) $status = 'Pending';
        else $status = 'Not verified';
    }

    include_once __DIR__.'/../Form/label.php';
    return '<div class="hr"></div>'.\Form\label('Email', "$email ($status)");

}
