<?php

function mysqli_query_exit ($mysqli, $sql) {
    $result = $mysqli->query($sql);
    if ($result === false) {
        include_once __DIR__.'/ErrorPage/internalServerError.php';
        ErrorPage\internalServerError($mysqli->error);
    }
    return $result;
}
