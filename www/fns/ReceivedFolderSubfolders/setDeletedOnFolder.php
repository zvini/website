<?php

namespace ReceivedFolderSubfolders;

function setDeletedOnFolder ($mysqli, $id_received_folders, $deleted) {
    $deleted = $deleted ? '1' : '0';
    $sql = "update received_folder_subfolders set deleted = $deleted"
        ." where id_received_folders = $id_received_folders";
    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);
}
