<?php

namespace BookmarkRevisions;

function add ($mysqli, $id_bookmarks, $id_users,
    $url, $title, $tags, $insert_time, $revision) {

    $url = $mysqli->real_escape_string($url);
    $title = $mysqli->real_escape_string($title);
    $tags = $mysqli->real_escape_string($tags);

    $sql = 'insert into bookmark_revisions'
        .' (id_bookmarks, id_users, url,'
        .' title, tags, insert_time, revision)'
        ." values ($id_bookmarks, $id_users, '$url',"
        ." '$title', '$tags', $insert_time, $revision)";

    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);

    return $mysqli->insert_id;

}
