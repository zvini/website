<?php

namespace BookmarkRevisions;

function setDeletedOnBookmark ($mysqli, $id_bookmarks, $deleted) {
    $deleted = $deleted ? '1' : '0';
    $sql = "update bookmark_revisions set deleted = $deleted"
        ." where id_bookmarks = $id_bookmarks";
    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);
}
