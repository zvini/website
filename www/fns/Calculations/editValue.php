<?php

namespace Calculations;

function editValue ($mysqli, $id, $value,
    $error, $error_char, $resolved_expression) {

    if ($value === null) {
        $value = 'null';
        $error = "'".$mysqli->real_escape_string($error)."'";
    } else {
        $error = $error_char = 'null';
    }

    $sql = "update calculations set value = $value,"
        ." error = $error, error_char = $error_char,"
        ." resolved_expression = '$resolved_expression' where id = $id";

    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);

}
