<?php

namespace PlacePoints;

function setDeletedOnPlace ($mysqli, $id_places, $deleted) {
    $deleted = $deleted ? '1' : '0';
    $sql = "update place_points set deleted = $deleted"
        ." where id_places = $id_places";
    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);
}
