<?php

namespace Files;

function editContent ($mysqli, $id, $size) {

    include_once __DIR__.'/../bytestr.php';
    $readable_size = bytestr($size);

    $sql = "update files set size = $size, readable_size = '$readable_size',"
        .' hashes_computed = 0, md5_sum = null, sha256_sum = null,'
        ." content_revision = content_revision + 1 where id_files = $id";

    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);

}
