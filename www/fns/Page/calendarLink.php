<?php

namespace Page;

function calendarLink ($content, $href, $day, $options = []) {
    return
        "<a href=\"$href\" class=\"clickable link image_link\">"
            .'<span class="icon calendar image_link-icon">'
                ."<span class=\"calendarIcon-day\">$day</span>"
            .'</span>'
            ."<span class=\"image_link-content\">$content</span>"
        .'</a>';
}
