<?php

namespace Page;

function disabledImageLink ($title, $iconName) {
    return
        "<div class=\"clickable link image_link\">"
            ."<div class=\"icon $iconName image_link-icon\"></div>"
            ."<div class=\"image_link-content colorText grey\">$title</div>"
        .'</div>';
}
