<?php

namespace Invitations;

function deleteAll ($mysqli) {
    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, 'delete from invitations');
}
