<?php

namespace WalletTransactions;

function setDeletedOnWallet ($mysqli, $id_wallets, $deleted) {
    $deleted = $deleted ? '1' : '0';
    $sql = "update wallet_transactions set deleted = $deleted"
        ." where id_wallets = $id_wallets";
    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);
}
