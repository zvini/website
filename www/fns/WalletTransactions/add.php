<?php

namespace WalletTransactions;

function add ($mysqli, $id_users, $id_wallets,
    $amount, $balance_after, $description, $insertApiKey) {

    $description = $mysqli->real_escape_string($description);
    $insert_time = $update_time = time();
    if ($insertApiKey === null) {
        $insert_api_key_id = $insert_api_key_name = 'null';
    } else {

        $insert_api_key_id = $insertApiKey->id;

        $name = $insertApiKey->name;
        $insert_api_key_name = "'".$mysqli->real_escape_string($name)."'";

    }

    $sql = 'insert into wallet_transactions'
        .' (id_users, id_wallets, amount, balance_after,'
        .' description, insert_time, update_time,'
        .' insert_api_key_id, insert_api_key_name)'
        ." values ($id_users, $id_wallets, $amount, $balance_after,"
        ." '$description', $insert_time, $update_time,"
        ." $insert_api_key_id, $insert_api_key_name)";

    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);

    return $mysqli->insert_id;

}
