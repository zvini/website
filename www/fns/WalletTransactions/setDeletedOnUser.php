<?php

namespace WalletTransactions;

function setDeletedOnUser ($mysqli, $id_users) {
    $sql = 'update wallet_transactions set deleted = 1'
        ." where id_users = $id_users";
    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);
}
