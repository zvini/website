<?php

namespace WalletTransactions;

function edit ($mysqli, $id, $amount,
    $balance_after, $description, $updateApiKey) {

    $description = $mysqli->real_escape_string($description);
    $update_time = time();
    if ($updateApiKey === null) {
        $update_api_key_id = $update_api_key_name = 'null';
    } else {

        $update_api_key_id = $updateApiKey->id;

        $name = $updateApiKey->name;
        $update_api_key_name = "'".$mysqli->real_escape_string($name)."'";

    }

    $sql = "update wallet_transactions set amount = $amount,"
        ." balance_after = $balance_after, description = '$description',"
        ." update_time = $update_time, update_api_key_id = $update_api_key_id,"
        ." update_api_key_name = $update_api_key_name,"
        ." revision = revision + 1 where id = $id";

    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);

}
