<?php

namespace NoteRevisions;

function setDeletedOnNote ($mysqli, $id_notes, $deleted) {
    $deleted = $deleted ? '1' : '0';
    $sql = "update note_revisions set deleted = $deleted"
        ." where id_notes = $id_notes";
    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);
}
