<?php

function file_sums ($filename, &$md5_sum, &$sha256_sum) {

    $md5_hash = hash_init('md5');
    $sha256_hash = hash_init('sha256');

    $time_limit = ini_get('max_execution_time');
    set_time_limit(0);

    $f = @fopen($filename, 'r');
    if ($f === false) return false;

    while (!feof($f)) {
        $chunk = fread($f, 1024 * 64);
        if ($chunk === false) return false;
        hash_update($md5_hash, $chunk);
        hash_update($sha256_hash, $chunk);
    }
    fclose($f);

    $md5_sum = hash_final($md5_hash);
    $sha256_sum = hash_final($sha256_hash);

    set_time_limit($time_limit);

    return true;

}
