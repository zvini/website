<?php

namespace TaskRevisions;

function setDeletedOnTask ($mysqli, $id_tasks, $deleted) {
    $deleted = $deleted ? '1' : '0';
    $sql = "update task_revisions set deleted = $deleted"
        ." where id_tasks = $id_tasks";
    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);
}
