<?php

namespace Folders;

function rename ($mysqli, $id, $name, $renameApiKey) {

    $name = $mysqli->real_escape_string($name);
    $rename_time = time();
    if ($renameApiKey === null) {
        $rename_api_key_id = $rename_api_key_name = 'null';
    } else {

        $rename_api_key_id = $renameApiKey->id;

        $keyName = $renameApiKey->name;
        $rename_api_key_name = "'".$mysqli->real_escape_string($keyName)."'";

    }

    $sql = "update folders set name = '$name', rename_time = $rename_time,"
        ." rename_api_key_id = $rename_api_key_id,"
        ." rename_api_key_name = $rename_api_key_name,"
        ." revision = revision + 1 where id_folders = $id";

    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);

}
