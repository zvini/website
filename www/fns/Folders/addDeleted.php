<?php

namespace Folders;

function addDeleted ($mysqli, $id, $id_users,
    $parent_id, $name, $insert_time, $rename_time, $revision) {

    $name = $mysqli->real_escape_string($name);

    $sql = 'insert into folders'
        .' (id_folders, id_users, parent_id,'
        .' name, insert_time, rename_time, revision)'
        ." values ($id, $id_users, $parent_id,"
        ." '$name', $insert_time, $rename_time, $revision)";

    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);

}
