<?php

namespace CalculationDepends;

function add ($mysqli, $id_users, $id_calculations, $depends) {
    $sql = 'insert into calculation_depends'
        .' (id_users, id_calculations, depend_id_calculations) values ';
    foreach ($depends as $i => $depend_id_calculations) {
        if ($i) $sql .= ', ';
        $sql .= "($id_users, $id_calculations, $depend_id_calculations)";
    }
    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);
}
