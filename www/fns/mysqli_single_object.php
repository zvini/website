<?php

function mysqli_single_object ($mysqli, $sql) {
    include_once __DIR__.'/mysqli_query_exit.php';
    $result = mysqli_query_exit($mysqli, $sql);
    $object = $result->fetch_object();
    $result->close();
    return $object;
}
