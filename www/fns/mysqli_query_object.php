<?php

function mysqli_query_object ($mysqli, $sql) {
    include_once __DIR__.'/mysqli_query_exit.php';
    $result = mysqli_query_exit($mysqli, $sql);
    $objects = [];
    while ($object = $result->fetch_object()) $objects[] = $object;
    $result->close();
    return $objects;
}
