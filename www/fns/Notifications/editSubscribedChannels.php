<?php

namespace Notifications;

function editSubscribedChannels ($mysqli, $ids, $channel_name) {
    $ids = join(', ', $ids);
    $channel_name = $mysqli->real_escape_string($channel_name);
    $sql = "update notifications set channel_name = '$channel_name'"
        ." where id_subscribed_channels in ($ids)";
    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);
}
