<?php

namespace BarChartBars;

function setDeletedOnBarChart ($mysqli, $id_bar_charts, $deleted) {
    $deleted = $deleted ? '1' : '0';
    $sql = "update bar_chart_bars set deleted = $deleted"
        ." where id_bar_charts = $id_bar_charts";
    include_once __DIR__.'/../mysqli_query_exit.php';
    mysqli_query_exit($mysqli, $sql);
}
