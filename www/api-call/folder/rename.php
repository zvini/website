<?php

include_once '../../../lib/defaults.php';

include_once '../fns/require_api_key.php';
require_api_key('folder/rename', 'can_write_files', $apiKey, $user, $mysqli);

include_once 'fns/require_folder.php';
$folder = require_folder($mysqli, $user);

include_once 'fns/require_folder_params.php';
require_folder_params($mysqli, $user->id_users,
    $folder->parent_id, $name, $folder->id_folders);

include_once '../../fns/Users/Folders/rename.php';
Users\Folders\rename($mysqli, $folder, $name, $changed, $apiKey);

header('Content-Type: application/json');
echo 'true';
