<?php

function get_values () {

    $key = 'install/mysql-settings/values';
    if (array_key_exists($key, $_SESSION)) return $_SESSION[$key];

    return [
        'host' => 'localhost',
        'username' => '',
        'password' => '',
        'db' => 'zvini',
        'create' => true,
        'check' => false,
    ];

}
