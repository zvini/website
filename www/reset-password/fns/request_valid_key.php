<?php

function request_valid_key ($mysqli, &$reset_password_key, &$user) {

    $fnsDir = __DIR__.'/../../fns';

    include_once "$fnsDir/require_guest_user.php";
    require_guest_user('../');

    include_once "$fnsDir/request_strings.php";
    list($key) = request_strings('key');

    include_once "$fnsDir/ResetPasswordKeys/getByKey.php";
    $reset_password_key = ResetPasswordKeys\getByKey($mysqli, $key);

    if (!$reset_password_key) return;

    include_once "$fnsDir/ResetPasswordKeys/expireHours.php";
    $expireTime = ResetPasswordKeys\expireHours() * 60 * 60;

    if ($reset_password_key->insert_time <= time() - $expireTime) return;

    $return = $reset_password_key->return;

    include_once "$fnsDir/Users/get.php";
    $user = Users\get($mysqli, $reset_password_key->id_users);

}
