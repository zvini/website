<?php

include_once '../../lib/defaults.php';

include_once '../fns/signed_user.php';
$user = signed_user();

include_once 'fns/unset_session_vars.php';
unset_session_vars();

include_once '../fns/Page/create.php';
include_once '../fns/Page/imageArrowLink.php';
include_once '../fns/Page/imageLink.php';
include_once '../fns/Page/sessionMessages.php';
$content = Page\create(
    [
        'title' => 'Home',
        'href' => '../home/#help',
    ],
    'Help',
    Page\sessionMessages('help/messages')
    .Page\imageLink('Install Link Handlers', 'install-link-handlers/', 'protocol', [
        'id' => 'install-link-handlers',
    ])
    .'<div class="hr"></div>'
    .Page\imageArrowLink('Leave Feedback', 'feedback/', 'feedback', [
        'id' => 'feedback',
    ])
    .'<div class="hr"></div>'
    .Page\imageArrowLink('API Documentation',
        'api-doc/', 'api-doc', ['id' => 'api-doc'])
    .'<div class="hr"></div>'
    .Page\imageArrowLink('Cross-site API Documentation',
        'cross-site-api-doc/', 'api-doc', ['id' => 'cross-site-api-doc'])
    .'<div class="hr"></div>'
    .Page\imageArrowLink('About Zvini', 'about-zvini/', 'zvini', [
        'id' => 'about-zvini',
    ])
);

include_once '../fns/echo_public_page.php';
echo_public_page($user, 'Help', $content, '../');
