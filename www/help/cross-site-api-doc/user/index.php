<?php

include_once '../../../../lib/defaults.php';

include_once '../fns/method_page.php';
include_once '../../../fns/Tags/maxNumber.php';
method_page('user', [], [
    'type' => 'string',
    'description' => 'The username of the current user.',
], []);
