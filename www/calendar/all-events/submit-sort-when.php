<?php

include_once '../../../lib/defaults.php';

include_once 'fns/submit_sort.php';
include_once '../../lib/mysqli.php';
submit_sort($mysqli,
    'event_time desc, start_hour, start_minute, insert_time desc',
    'event time');
