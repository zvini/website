(function (base, time, timezone) {

    var clock = Clock(time, timezone)

    window.batteryAndClock = {
        onClockUpdate: clock.onUpdate,
        unClockUpdate: clock.unUpdate,
    }

})(base, time, timezone)
