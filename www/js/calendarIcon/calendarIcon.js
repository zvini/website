(function (batteryAndClock) {

    function clockUpdate (date) {
        var day = date.getUTCDate().toString()
        if (day !== dayNode.nodeValue) dayNode.nodeValue = day
    }

    var dayElement = document.querySelector('.calendarIcon-today')
    var dayNode = dayElement.firstChild
    batteryAndClock.onClockUpdate(clockUpdate)

})(batteryAndClock)
