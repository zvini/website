var uglifyJs = require('uglify-js')

module.exports = function (source) {

    var result = uglifyJs.minify(source, {
        toplevel: true,
        warnings: true,
        output: { max_line_len: 1024 },
    })

    var warnings = result.warnings
    if (warnings !== undefined) console.log(warnings)

    return result.code

}
