function create_calendar_icon_today (parentNode, response, additionalClass) {
    if (additionalClass === undefined) additionalClass = ''
    ui.Element(parentNode, 'span', function (span) {
        span.className = 'icon calendar' + additionalClass
        ui.Element(span, 'span', function (span) {
            span.className = 'calendarIcon-day calendarIcon-today'
            ui.Text(span, new Date(response.time).getUTCDate())
        })
    })
}
