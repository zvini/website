<?php

function submit_sort ($mysqli, $order_by, $what) {

    $fnsDir = __DIR__.'/../../fns';

    include_once "$fnsDir/require_same_domain_referer.php";
    require_same_domain_referer('./');

    include_once "$fnsDir/require_user.php";
    $user = require_user('../');

    include_once "$fnsDir/Users/Notes/editOrderBy.php";
    include_once '../lib/mysqli.php';
    Users\Notes\editOrderBy($mysqli, $user->id_users, $order_by);

    unset($_SESSION['notes/errors']);
    $_SESSION['notes/messages'] = ["The list is now sorted by $what."];

    include_once "$fnsDir/redirect.php";
    include_once "$fnsDir/ItemList/listUrl.php";
    redirect(ItemList\listUrl('./'));

}
