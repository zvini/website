#!/usr/bin/php
<?php

chdir(__DIR__);
include_once '../../lib/cli.php';
include_once '../../lib/defaults.php';
include_once '../lib/mysqli.php';

include_once '../fns/Tables/ensureAll.php';
echo Tables\ensureAll($mysqli);

$sql = 'update cross_site_api_keys k'
    .' join users u on u.id_users = k.id_users'
    .' set k.username = u.username';
include_once '../fns/mysqli_query_exit.php';
mysqli_query_exit($mysqli, $sql);

echo "Done\n";
