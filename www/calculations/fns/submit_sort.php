<?php

function submit_sort ($mysqli, $order_by, $what) {

    $fnsDir = __DIR__.'/../../fns';

    include_once "$fnsDir/require_same_domain_referer.php";
    require_same_domain_referer('./');

    include_once "$fnsDir/require_user.php";
    $user = require_user('../');

    include_once "$fnsDir/Users/Calculations/editOrderBy.php";
    Users\Calculations\editOrderBy($mysqli, $user->id_users, $order_by);

    unset($_SESSION['calculations/errors']);
    $_SESSION['calculations/messages'] = ["The list is now sorted by $what."];

    include_once "$fnsDir/redirect.php";
    include_once "$fnsDir/ItemList/listUrl.php";
    redirect(ItemList\listUrl('./'));

}
