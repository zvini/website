<?php

include_once '../../lib/defaults.php';

include_once 'fns/require_user_with_password.php';
$user = require_user_with_password();

$base = '../';
$fnsDir = '../fns';

include_once 'fns/unset_session_vars.php';
unset_session_vars();

include_once 'fns/create_api_keys_link.php';
include_once 'fns/create_connections_link.php';
include_once 'fns/create_tokens_link.php';
include_once 'fns/create_options_panel.php';
include_once "$fnsDir/export_date_ago.php";
include_once "$fnsDir/Form/label.php";
include_once "$fnsDir/Page/create.php";
include_once "$fnsDir/Page/infoText.php";
include_once "$fnsDir/Page/sessionMessages.php";
include_once "$fnsDir/Page/thumbnailLink.php";
include_once "$fnsDir/Page/thumbnails.php";
include_once "$fnsDir/UserViewPage/emailItem.php";
include_once "$fnsDir/UserViewPage/fullNameItem.php";
include_once "$fnsDir/UserViewPage/storageItem.php";
include_once "$fnsDir/UserViewPage/signinsItem.php";
include_once "$fnsDir/UserViewPage/themeItem.php";
include_once "$fnsDir/UserViewPage/timezoneItem.php";
$content =
    Page\create(
        [
            'title' => 'Home',
            'href' => '../home/#account',
            'localNavigation' => true,
        ],
        'Account',
        Page\sessionMessages('account/messages')
        .Page\thumbnails([
            create_api_keys_link($user),
            Page\thumbnailLink('Authentication History',
                'signins/', 'sign-ins', ['id' => 'signins']),
            create_connections_link($user),
            create_tokens_link($user),
        ])
        .'<div class="hr"></div>'
        .Form\label('Username', $user->username)
        .UserViewPage\emailItem($user)
        .UserViewPage\fullNameItem($user)
        .UserViewPage\timezoneItem($user)
        .UserViewPage\themeItem($user)
        .UserViewPage\storageItem($user)
        .UserViewPage\signinsItem($user)
        .Page\infoText('Account created '
            .export_date_ago($user->insert_time, true).'.')
    )
    .create_options_panel($user);

include_once "$fnsDir/compressed_js_script.php";
include_once "$fnsDir/echo_user_page.php";
echo_user_page($user, 'Account', $content, $base, [
    'scripts' => compressed_js_script('dateAgo', $base),
]);
