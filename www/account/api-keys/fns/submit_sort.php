<?php

function submit_sort ($mysqli, $order_by, $what) {

    $fnsDir = __DIR__.'/../../../fns';

    include_once "$fnsDir/require_same_domain_referer.php";
    require_same_domain_referer('./');

    include_once __DIR__.'/../../fns/require_user_with_password.php';
    $user = require_user_with_password('../');

    include_once "$fnsDir/Users/ApiKeys/editOrderBy.php";
    Users\ApiKeys\editOrderBy($mysqli, $user->id_users, $order_by);

    unset($_SESSION['account/api-keys/errors']);
    $_SESSION['account/api-keys/messages'] = [
        "The list is now sorted by $what.",
    ];

    include_once "$fnsDir/redirect.php";
    include_once "$fnsDir/ItemList/listUrl.php";
    redirect(ItemList\listUrl('./'));

}
